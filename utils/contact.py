class Rectangle:
    def __init__(self, lu_x, lu_y, w, h, offset):
        self.lu_x = lu_x - offset  # left upper corner x
        self.lu_y = lu_y - offset  # left upper corner y
        self.w = w + offset  # width
        self.h = h + offset  # height
        self.c_x, self.c_y = self.__get_center()  # center x y

    def __get_center(self):
        return self.lu_x + self.w / 2, self.lu_y + self.h / 2


def get_distance(rectangle1: Rectangle, rectangle2: Rectangle) -> float:
    distance = max(abs(rectangle1.c_x - rectangle2.c_x) - (rectangle1.w + rectangle2.w) / 2,
                   abs(rectangle1.c_y - rectangle2.c_y) - (rectangle1.h + rectangle2.h) / 2)
    return distance


def find_contacts(offset: int, colors: dict) -> dict:
    result = {}
    for color1, obj1 in colors.items():
        rectangle1 = Rectangle(obj1['x'], obj1['y'], obj1['w'], obj1['h'], offset)
        for color2, obj2 in colors.items():
            rectangle2 = Rectangle(obj2['x'], obj2['y'], obj2['w'], obj2['h'], offset)
            if color1 != color2 and get_distance(rectangle1, rectangle2) < 0:
                if color1 not in result:
                    result[color1] = []
                result[color1].append(color2)
    return result
