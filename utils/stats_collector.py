class StatsCollector:
    def __init__(self, file_path):
        self.file_path = file_path
        self.__detected_ball_id = 0
        self.__detected_balls = dict()
        self.__infected_balls = dict()

    def ball_detected(self, color):
        if color not in self.__detected_balls:
            self.__detected_ball_id += 1
            self.__detected_balls[color] = self.__detected_ball_id

        return self.__detected_balls[color]

    def red_ball_contact_detected(self, colors: list):
        for color in colors:
            self.__infected_balls[color] = self.__get_color_id(color)

    def __get_color_id(self, color):
        return self.__detected_balls[color]

    def get_infected_balls(self) -> dict:
        return self.__infected_balls

    def export_stats(self):
        with open(self.file_path, 'w') as f:
            f.write(f'Video processing results\n')
            f.write('-'*50 + '\n')
            f.write(f'Total balls count: {len(self.__detected_balls)}\n')
            f.write('Red ball detected: '
                    f'{"yes" if "red" in self.__detected_balls else "no"} ({self.__detected_balls.get("red", "")})\n')
            f.write('Infected balls: '
                    f'{len(self.__infected_balls)} ({", ".join(str(num) for num in self.__infected_balls.values())})\n')
