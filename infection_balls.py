import sys
import os
from pathlib import Path

import cv2
import numpy as np

from utils import StatsCollector
from utils.contact import find_contacts

COLORS = {
    'green': (100, 100, 30, 255, 255, 80),
    'yellow': (196, 0, 18, 255, 255, 36),
    'blue': (0, 0, 89, 255, 255, 125),
    'red': (80, 100, 0, 255, 255, 10)
}

path = os.path.dirname(os.path.abspath(__file__))


def nothing(*arg):
    pass


def detect(file_path):
    file_name = Path(file_path).stem

    stats_collector = StatsCollector(os.path.join(path, f'results_{file_name}.txt'))
    vid_capture = cv2.VideoCapture(file_path)

    width = int(vid_capture.get(cv2.CAP_PROP_FRAME_WIDTH))
    height = int(vid_capture.get(cv2.CAP_PROP_FRAME_HEIGHT))

    print(f'Loaded the video {width}×{height}\nStart processing frames...')

    frames = []

    while vid_capture.isOpened():
        is_retrieved, frame = vid_capture.read()

        if not is_retrieved:
            break

        frame_hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
        detected = dict()

        for color in COLORS:
            # Get values for the current color
            low_hue = COLORS[color][2]
            low_sat = COLORS[color][1]
            low_val = COLORS[color][0]
            high_hue = COLORS[color][5]
            high_sat = COLORS[color][4]
            high_val = COLORS[color][3]

            # HSV values to define a color range we want to create a mask from
            color_low = np.array([low_hue, low_sat, low_val])
            color_high = np.array([high_hue, high_sat, high_val])
            mask = cv2.inRange(frame_hsv, color_low, color_high)

            contours, hierarchy = cv2.findContours(mask, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

            contour_sizes = [(cv2.contourArea(contour), contour) for contour in contours]

            if contour_sizes:
                ball_id = stats_collector.ball_detected(color)
                biggest_contour = max(contour_sizes, key=lambda s: s[0])[1]
                x, y, w, h = cv2.boundingRect(biggest_contour)

                detected[color] = {
                    'x': x,
                    'y': y,
                    'w': w,
                    'h': h
                }

                cv2.putText(frame, str(ball_id), (x + 10, y + 10), cv2.FONT_HERSHEY_SIMPLEX, 2, (255, 255, 255), 5)

            frames.append(frame)

        contacts = find_contacts(50, detected)
        if 'red' in contacts:
            stats_collector.red_ball_contact_detected(contacts['red'])

        for infected_ball in list(stats_collector.get_infected_balls()):
            if infected_ball in contacts:
                stats_collector.red_ball_contact_detected(contacts[infected_ball])


    vid_capture.release()

    save_video(file_name, width, height, frames)
    stats_collector.export_stats()


def save_video(file_name, width, height, frames):
    print('Saving a processed video...')

    out = cv2.VideoWriter(f'{file_name}_processed.mp4', cv2.VideoWriter_fourcc(*'mp4v'), 60.0, (width, height))
    for frame in frames:
        out.write(frame)
    out.release()


def main():
    if len(sys.argv) >= 2:
        detect(sys.argv[1])
    else:
        print('Pass the path to a video file as the first argument')


if __name__ == '__main__':
    main()
